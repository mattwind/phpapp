# phpApp#

My attempt at a PHP framework. The folder structure should represent the namespace hierarchy, and I should write more documentation.

### What is this repository for? ###

* Building custom Web Applications.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* git
* php5
* sqlite3

### App ###

This is the main application files, custom CSS, JavaScript, and views.

### Data ###

Currently storing SQLite3 database files, could store XML, and web-cache

### Domain ###

This folder contains a collection of interfaces, and models.

### Repo ###

The repository has all the persistent database, or data layer files.

### Vendors ###

Storing all third-party UI code, like bootstrap CSS, and jQuery libraries.