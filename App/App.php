<?php

namespace App
{

	use \Repo\MyPdo as Repo;
	use \Domain\Models\Template as View;

	$database = new Repo();

	$view = new View("App/Views/index.php");
	$view->title = $database->helloWorld();

	/*
	 * Get data from the user array
	 */
	foreach ($database->listUsers() as $array)
	{
		$users[] = $array['username'];
	}

	$view->users = implode("<br />", $users);
	$view->footer = $database->copyright();

	echo $view;

}

?>
