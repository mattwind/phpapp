<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?=$title?></title>

		<!-- Bootstrap -->
		<link href="Vendors/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="App/Assets/css/sticky-footer.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
<body>

	<!-- Begin page content -->
	<div class="container">
		<div class="page-header">
			<h1><?=$title?></h1>
		</div>

		<p class="lead"><?=$users;?></p>
	</div>  <!-- /container -->

		<!-- Site footer -->
		<div class="footer">
			<div class="container">
				<p class="text-muted"><?=$footer?> | 
				<a href="Vendors/phpSQLiteAdmin/">phpSQLite Admin</a></p>
			</div>
		</div>


	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="Vendors/jquery/2.1.1/jquery.min.js"></script>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="Vendors/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
