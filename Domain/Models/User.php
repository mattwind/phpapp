<?php

namespace Domain\Models
{

    Class User implements \Domain\Interfaces\User
    {
		public function UserRole()
		{
			$this->userRole = "Guest";
		}
    }

    Class Admin extends User
    {
		public function UserRole()
		{
			$this->userRole = "Admin";
		}

		public function DeleteUser( $userID )
		{
			return $this->username;
		}
    }

    Class Moderator extends User
    {
		public function UserRole()
		{
			$this->userRole = "Moderator";
		}
			
		public function EditUser( $userID )
		{
			return $this->username;
		}
	}

}

?>
