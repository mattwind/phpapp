<?php

namespace Domain\Interfaces
{

	interface Database
	{
		public function listUsers();
		public function copyright();
		public function helloWorld();
	}

}

?>
