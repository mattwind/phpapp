<?php

/*
 * Debug All
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

/*
 * Autoload Class
 */

function my_autoload ($class) {
	$class = str_replace('\\', '/', $class);
	include(__DIR__ . "/" . $class . ".php");
}

spl_autoload_register("my_autoload");

/*
 * Load App
 */

include("App/App.php");

?>
