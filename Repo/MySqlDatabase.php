<?php

namespace Repo
{

	Class MySqlDatabase implements \Domain\Interfaces\Database
	{
		public function listUsers()
		{
			$users = [ "Matt", "Morgan", "Ryan" ];
			return $users;
		}

		public function helloWorld()
		{
			return "Hello World";
		}

		public function copyright()
		{
			return "&copy; Copyright 2014";
		}
	}

}

?>
