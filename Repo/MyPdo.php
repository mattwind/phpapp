<?php

namespace Repo
{

	class MyPdo implements \Domain\Interfaces\Database
	{
		public $db;

		function __construct()
		{
			$this->db = new \PDO('sqlite:Data/database.db');
		}

		public function listUsers()
		{
			$results  = $this->db->query("SELECT * FROM users");
			$array	  = $results->fetchAll();

			return $array;
		}

		public function helloWorld()
		{
			return "Hello World";
		}

		public function copyright()
		{
			return "&copy Copyright 2014";
		}

	}

}
